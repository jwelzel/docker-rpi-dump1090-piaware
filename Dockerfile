FROM jwelzel/rpi-rtlsdr-base:latest

MAINTAINER Jochen Welzel

# The following packages are required for piaware compilation

WORKDIR /tmp

RUN wget $(wget -qO- http://flightaware.com/adsb/piaware/install | egrep wget | awk '{print $3}' | awk -F\< '{print $1}') && \
    dpkg -i piaware*.deb

WORKDIR /tmp

RUN apt-get update && apt-get install -y --no-install-recommends \
    piaware \
    dump1090-fa

COPY piaware_config /root/.piaware
COPY start-dump1090-piaware.sh /root/start-dump1090-piaware.sh

RUN mkdir /run/dump1090-fa

RUN piaware-config allow-auto-updates yes &&\
    piaware-config allow-manual-updates yes

WORKDIR /tmp

# Cleanup
RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

EXPOSE 80 8080 30001 30002 30003 30004 30005 30104

CMD ["/root/start-dump1090-piaware.sh"]
