# Docker Container with dump1090 + piaware + rtlsdr 

### ENV Variables
```
USERNAME=username
PASSWORD=password
LAT=XX.XXXXXX
LONG=XX.XXXXXX
PPM=1
FEEDERID=XXXXXXXXXXXXXXXXXXXXXXXX
DEVICE_SERIAL=00000001
```

### Thanks
* To Frederik Granna for his [blog post](http://www.sysrun.io/2015/11/20/a-complete-docker-rpi-rtl-sdr-adsbacars-solution/) and some docker/code inspirations
* To Glenn Stewart for his [piaware containers](https://hub.docker.com/u/inodes/) 
